<?php
/**
 * Plugin Name: Inventario VINs retail
 * Plugin URI: https://iterando.mx
 * Description: Plugin que gestiona a los VINs de inventario de tiendas de retail para Motodrive.
 * Version: 1.0
 * Author: Iterando
 * Author URI: https://iterando.mx
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

class VinRetail {

    function __construct() {
        add_action('init', array($this, 'vins_cpt'));
        add_shortcode('colnal-miembros-all', array($this, 'all_members'));
        add_shortcode('colnal-carousel-full', array($this, 'carousel_full'));
        add_filter('single_template', array($this, 'single_member' ));
    }

    function activate() {
        // Debe generarse un CPT
        // Flush rewrite rules
        $this->vins_cpt();
        $this->all_members();

        flush_rewrite_rules();
    }

    function deactivate() {
        // Flush rewrite rules
        flush_rewrite_rules();
    }

    function uninstall() {
        // Debe eliminarse los CPT
        // Eliminar todos los datos del plugin de la base de datos
    }

    function all_members() {
        $dir = plugin_dir_path(__FILE__) . '/frontend/'; 

        if (is_user_logged_in()) {
            show_admin_bar(true);
        }

        wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        wp_enqueue_style('colnal-members', plugins_url('/assets/css/colnal-members.css', __FILE__));

        ob_start();
        include($dir."main.php");
        return ob_get_clean();
    }

    function cf_search_join( $join ) {
        global $wpdb;

        if ( is_search() ) {
            $join .=' LEFT JOIN '.$wpdb->postmeta. ' cfmeta ON '. $wpdb->posts . '.ID = cfmeta.post_id ';
        }

        return $join;
    }

    function cf_search_where( $where ) {
        global $pagenow, $wpdb;

        if ( is_search() ) {
            $where = preg_replace(
                "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
                "(".$wpdb->posts.".post_title LIKE $1) OR (cfmeta.meta_value LIKE $1)", $where );
        }

        return $where;
    }

    function carousel_full() {
        $dir = plugin_dir_path(__FILE__) . '/frontend/';

        if (is_user_logged_in()) {
            show_admin_bar(true);
        }

        wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
        // wp_enqueue_style('slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
        wp_enqueue_style('colnal-members', plugins_url('/assets/css/colnal-members.css', __FILE__));

        wp_enqueue_script( 'bootstrap-popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script( 'bootstrap-core','https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array( 'jquery' ),'',true );

        include($dir."slider.php");
    }

    function single_member() {
        global $post;

        $all_custom_post_types = get_post_types( array ( '_builtin' => FALSE ) );

        // there are no custom post types
        if ( empty ( $all_custom_post_types ) )
            return FALSE;

        $custom_types      = array_keys( $all_custom_post_types );
        $current_post_type = get_post_type( $post );

        // could not detect current type
        if ( !array_search($current_post_type, $custom_types) ){
			include(plugin_dir_path(__FILE__) . '../../themes/norebro-child/single.php'); exit();
        } else {
            if($current_post_type == 'vins'){
                wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
                wp_enqueue_style('colnal-members', plugins_url('/assets/css/colnal-members.css', __FILE__));

                wp_enqueue_script( 'bootstrap-popper','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array( 'jquery' ),'',true );
                wp_enqueue_script( 'bootstrap-core','https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array( 'jquery' ),'',true );

                include plugin_dir_path(__FILE__) . '/frontend/single-vins.php'; exit();
            }
        }
    }

    /*
     *
     */

    function vins_cpt() {
		
        register_post_type('vins', array(
            'public' => 'true',
            'has_archive' => true,
            'show_in_menu' => true,
            'labels' => array(
                'name' => 'VINs',
                'singular_name' => 'VIN',
            ),
            'rewrite' => array( 'with_front' => false ),
            'supports' => array( 'title' ),
			'taxonomies'          => array( 'retail' )
        ));

        add_action( 'add_meta_boxes', array($this, 'vins_register_meta_boxes' ));
        add_action( 'admin_enqueue_scripts', array($this, 'enqueue_date_picker' ) );

        add_action( 'save_post', function($post_id){
			

            if(isset($_POST['motodrv-moto-color'])){
                $data = htmlspecialchars($_POST['motodrv-moto-color']);
                update_post_meta($post_id, 'motodrv-moto-color', $data);
            }

            if(isset($_POST['motodrv-moto-model'])){
                $data = htmlspecialchars($_POST['motodrv-moto-model']);
                update_post_meta($post_id, 'motodrv-moto-model', $data);
            }

            if(isset($_POST['motodrv-moto-repuve'])){
                $data = htmlspecialchars($_POST['motodrv-moto-repuve']);
                update_post_meta($post_id, 'motodrv-moto-repuve', $data);
            }

            if(isset($_POST['motodrv-moto-customs'])){
                $data = htmlspecialchars($_POST['motodrv-moto-customs']);
                update_post_meta($post_id, 'motodrv-moto-customs', $data);
            }

            if(isset($_POST['motodrv-moto-pedimento'])){
                $data = htmlspecialchars($_POST['motodrv-moto-pedimento']);
                update_post_meta($post_id, 'motodrv-moto-pedimento', $data);
            }

            if(isset($_POST['motodrv-pedimento-date'])){
                $data = htmlspecialchars($_POST['motodrv-pedimento-date']);
                update_post_meta($post_id, 'motodrv-pedimento-date', $data);
            }

            if(isset($_POST['motodrv-moto-client'])){
                $data = htmlspecialchars($_POST['motodrv-moto-client']);
                update_post_meta($post_id, 'motodrv-moto-client', $data);
            }

            if(isset($_POST['motodrv-moto-client-email'])){
                $data = htmlspecialchars($_POST['motodrv-moto-client-email']);
                update_post_meta($post_id, 'motodrv-moto-client-email', $data);
            }

            if(isset($_POST['motodrv-moto-status'])){
                $data = htmlspecialchars($_POST['motodrv-moto-status']);
                update_post_meta($post_id, 'motodrv-moto-status', $data);
            }

            if(isset($_POST['motodrv-activation-date'])){
                $data = htmlspecialchars($_POST['motodrv-activation-date']);
                update_post_meta($post_id, 'motodrv-activation-date', $data);
            }

            if(isset($_POST['motodrv-sale-date'])){
                $data = htmlspecialchars($_POST['motodrv-sale-date']);
                update_post_meta($post_id, 'motodrv-sale-date', $data);
            }

            if(isset($_POST['motodrv-moto-retail'])){
                $data = htmlspecialchars($_POST['motodrv-moto-retail']);
                update_post_meta($post_id, 'motodrv-moto-retail', $data);
            }
			
			if(isset($_POST['motodrv-moto-state'])){
                $data = htmlspecialchars($_POST['motodrv-moto-state']);
                update_post_meta($post_id, 'motodrv-moto-state', $data);
            }
			
			if(isset($_POST['motodrv-moto-store'])){
                $data = htmlspecialchars($_POST['motodrv-moto-store']);
                update_post_meta($post_id, 'motodrv-moto-store', $data);
            }
			

        });
    }
	
	
    function enqueue_date_picker() {
        wp_enqueue_script( 'jquery-ui-datepicker' );
        wp_register_style( 'jquery-ui', 'http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css' );
        wp_enqueue_style( 'jquery-ui' );
    }

    function vins_register_meta_boxes() {
		//add_meta_box( 'motodrv-moto-color', 'Color de moto', array($this, 'hcf_display_color'), 'vins' );
		//add_meta_box( 'motodrv-moto-model', 'Modelo de moto', array($this, 'hcf_display_model'), 'vins' );
		add_meta_box( 'motodrv-moto-pedimento', 'Número de pedimento', array($this, 'hcf_display_pedimento'), 'vins' );
		add_meta_box( 'motodrv-pedimento-date', 'Fecha de pedimento', array($this, 'hcf_display_pedimento_date'), 'vins' );
		add_meta_box( 'motodrv-moto-repuve', 'REPUVE', array($this, 'hcf_display_repuve'), 'vins' );
		add_meta_box( 'motodrv-moto-customs', 'Aduana', array($this, 'hcf_display_customs'), 'vins' );
		add_meta_box( 'motodrv-moto-status', 'Estatus', array($this, 'hcf_display_status'), 'vins' );
		add_meta_box( 'motodrv-sale-date', 'Fecha de venta', array($this, 'hcf_display_sale_date'), 'vins' );
		add_meta_box( 'motodrv-activation-date', 'Fecha de activacion', array($this, 'hcf_display_activation_date'), 'vins' );
		add_meta_box( 'motodrv-moto-client', 'Nombre del cliente', array($this, 'hcf_display_client'), 'vins' );
		add_meta_box( 'motodrv-moto-client-email', 'Correo del cliente', array($this, 'hcf_display_client_email'), 'vins' );
		//add_meta_box( 'motodrv-moto-retail', 'Retail', array($this, 'hcf_display_retail'), 'vins' );
		//add_meta_box( 'motodrv-moto-state', 'Estado', array($this, 'hcf_display_state'), 'vins' );
		//add_meta_box( 'motodrv-moto-store', 'Tienda', array($this, 'hcf_display_store'), 'vins' );
		
    }
    
    function hcf_display_color($post) {
        $data = get_post_meta($post->ID, 'motodrv-moto-color', true);
        echo '<input type="text" id="moto-color" style="width: 100%" name="motodrv-moto-color" value="'.$data.'"/>';
    }

    function hcf_display_model($post) {
        $data = get_post_meta($post->ID, 'motodrv-moto-model', true);
        echo '<input type="text" id="moto-model" style="width: 100%" name="motodrv-moto-model" value="'.$data.'"/>';
    }

    function hcf_display_repuve($post) {
        $data = get_post_meta($post->ID, 'motodrv-moto-repuve', true);
        echo '<input type="text" id="moto-repuve" style="width: 100%" name="motodrv-moto-repuve" value="'.$data.'"/>';
    }

    function hcf_display_customs( $post ) {
        $data= get_post_meta($post->ID, 'motodrv-moto-customs' , true );
        echo '<input type="text" id="moto-customs" style="width: 100%" name="motodrv-moto-customs" value="'.$data.'"/>';
    }

    function hcf_display_pedimento( $post ) {
        $data= get_post_meta($post->ID, 'motodrv-moto-pedimento' , true );
        echo '<input type="text" id="moto-pedimiento" style="width: 100%" name="motodrv-moto-pedimento" value="'.$data.'"/>';
    }
	
    function hcf_display_pedimento_date( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-pedimento-date' , true );

        echo '<input type="text" id="datepicker-pedimento" style="width: 100%" name="motodrv-pedimento-date" value="'.$data.'">';
        echo '<script>
            jQuery(document).ready(function($) {
                $("#datepicker-pedimento").datepicker();
            });
        </script>';
    }
	
	function hcf_display_client( $post ) {
        $data= get_post_meta($post->ID, 'motodrv-moto-client' , true );
        echo '<input type="text" id="moto-client" style="width: 100%" name="motodrv-moto-client" value="'.$data.'"/>';
    }
	
	function hcf_display_client_email( $post ) {
        $data= get_post_meta($post->ID, 'motodrv-moto-client-email' , true );
        echo '<input type="text" id="moto-client-email" style="width: 100%" name="motodrv-moto-client-email" value="'.$data.'"/>';
    }
	
	function hcf_display_status( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-moto-status', true);
        $option_values = array("available" => "Disponible", "sold" => "Vendida", "waiting" => "Esperando", "active"=>"Activada");

        echo '<select id="motodrv-moto-status"  style="width: 100%" name="motodrv-moto-status">';
        echo '<option value="">Selecciona un estatus...</option>';
        foreach($option_values as $k => $v) {
            if($k == $data) {
                echo '<option value="'.$k.'" selected>'.$v.'</option>';
            } else {
                echo '<option value="'.$k.'">'.$v.'</option>';
            }
        }
        echo '</select>';
    }
	
	function hcf_display_activation_date( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-activation-date' , true );

        echo '<input type="text" id="datepicker-activation" style="width: 100%" name="motodrv-activation-date" value="'.$data.'">';
        echo '<script>
            jQuery(document).ready(function($) {
                $("#datepicker-activation").datepicker();
            });
        </script>';
    }
	
	function hcf_display_sale_date( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-sale-date' , true );

        echo '<input type="text" id="datepicker-sale" style="width: 100%" name="motodrv-sale-date" value="'.$data.'">';
        echo '<script>
            jQuery(document).ready(function($) {
                $("#datepicker-sale").datepicker();
            });
        </script>';
    }
	
	function hcf_display_retail( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-moto-retail', true);
        $option_values = array("alive" => "Activo", "dead" => "Fallecido");
        echo '<select id="motodrv-moto-retail"  style="width: 100%" name="motodrv-moto-retail">';
        echo '<option value="">Selecciona un retail...</option>';
        foreach($option_values as $k => $v) {
            if($k == $data) {
                echo '<option value="'.$k.'" selected>'.$v.'</option>';
            } else {
                echo '<option value="'.$k.'">'.$v.'</option>';
            }
        }
        echo '</select>';
    }
	
	function hcf_display_state( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-moto-state', true);
        $option_values = array("alive" => "Activo", "dead" => "Fallecido");

        echo '<select id="motodrv-moto-state"  style="width: 100%" name="motodrv-moto-state">';
        echo '<option value="">Selecciona un estado...</option>';
        foreach($option_values as $k => $v) {
            if($k == $data) {
                echo '<option value="'.$k.'" selected>'.$v.'</option>';
            } else {
                echo '<option value="'.$k.'">'.$v.'</option>';
            }
        }
        echo '</select>';
    }
	
	function hcf_display_store( $post ) {
        $data = get_post_meta($post->ID, 'motodrv-moto-store', true);
        $option_values = array("alive" => "Activo", "dead" => "Fallecido");

        echo '<select id="motodrv-moto-store"  style="width: 100%" name="motodrv-moto-store">';
        echo '<option value="">Selecciona una tienda...</option>';
        foreach($option_values as $k => $v) {
            if($k == $data) {
                echo '<option value="'.$k.'" selected>'.$v.'</option>';
            } else {
                echo '<option value="'.$k.'">'.$v.'</option>';
            }
        }
        echo '</select>';
    }
}

//hook into the init action and call create_book_taxonomies when it fires

add_action( 'init', 'create_models_hierarchical_taxonomy', 0 );
add_action( 'init', 'create_colors_hierarchical_taxonomy', 0 );

add_action( 'init', 'create_retails_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it topics for your posts
function create_models_hierarchical_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Modelos', 'taxonomy general name' ),
    'singular_name' => _x( 'Modelo', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar modelo' ),
    'all_items' => __( 'Todos los modelos' ),
    'parent_item' => __( 'Modelo padre' ),
    'parent_item_colon' => __( 'Modelo padre:' ),
    'edit_item' => __( 'Editar modelo' ), 
    'update_item' => __( 'Actualizar modelo' ),
    'add_new_item' => __( 'Nuevo modelo' ),
    'new_item_name' => __( 'Nombre del modelo' ),
    'menu_name' => __( 'Modelos' ),
  );    
 
// Now register the taxonomy
  register_taxonomy('modelo',array('vins'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'modelo' ),
  ));
 
}

function create_colors_hierarchical_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Colores', 'taxonomy general name' ),
    'singular_name' => _x( 'Color', 'taxonomy singular name' ),
    'search_items' =>  __( 'Buscar color' ),
    'all_items' => __( 'Todos los colores' ),
    'edit_item' => __( 'Editar color' ), 
    'update_item' => __( 'Actualizar color' ),
    'add_new_item' => __( 'Nuevo color' ),
    'new_item_name' => __( 'Nombre del color' ),
    'menu_name' => __( 'Colores' ),
  );    
 
// Now register the taxonomy
  register_taxonomy('color',array('vins'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'color' ),
  ));
 
}

function create_retails_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Retails', 'taxonomy general name' ),
    'singular_name' => _x( 'Retail', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Retails' ),
    'all_items' => __( 'All Retails' ),
    'parent_item' => __( 'Parent Retial' ),
    'parent_item_colon' => __( 'Parent Retail:' ),
    'edit_item' => __( 'Edit Retail' ), 
    'update_item' => __( 'Update Retail' ),
    'add_new_item' => __( 'Add New Retail' ),
    'new_item_name' => __( 'New Retail Name' ),
    'menu_name' => __( 'Retails' ),
  );    
 
// Now register the taxonomy
  register_taxonomy('retail',array('vins'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'retail' ),
  ));
 
}

if(class_exists('VinRetail')){
    $vinretailinventory = new VinRetail();
}

// Activación del Plugin
register_activation_hook(__FILE__, array($vinretailinventory, 'activate'));

// Desactivación del Plugin
register_deactivation_hook(__FILE__, array($vinretailinventory, 'deactivate'));

// Desinstalación del Plugin