<?php
/**
 * Created by PhpStorm.
 * User: rzerostern
 * Date: 7/30/19
 * Time: 6:25 PM
 */
$area_values = array("1" => "Ciencias Biológicas y de la Salud", "2" => "Artes y Letras", "3" => "Ciencias Exactas", "4" => "Ciencias Sociales y Humanidades");
$backgroundStyle = '';
$item = 0;

$loop = new WP_Query(array(
    'post_type' => 'integrantes',
    'posts_per_page' => 10,
    'orderby'        => 'rand'
));
?>
<div class="container-fluid colnal-slider-container">
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <?php
            if($loop->have_posts()) :
                while($loop->have_posts()): $loop->the_post();

                $area = get_post_meta(get_the_ID(), 'colnal-member-study-area', true);
                switch($area) {
                    case "1": $backgroundStyle = 'bio-tag-color'; break;
                    case "2": $backgroundStyle = 'arts-tag-color'; break;
                    case "3": $backgroundStyle = 'exact-tag-color'; break;
                    case "4": $backgroundStyle = 'social-tag-color'; break;
                }
            ?>
            <div class="carousel-item item-integrantes-colnal <?= ($item === 0) ? 'active' : '' ?> <?= $backgroundStyle; ?>" data-interval="10000">
                <div class="container">
                    <div class="row header-integrantes-colnal">
                        <div class="col-lg-6">
                            <h1 class="colnal-title">Integrantes</h1>
                        </div>
                        <div class="col-lg-6 text-right txt-left-responsive">
                            <a href="/nuestros-integrantes" class="show-all-events-colnal show-all-events-colnal-black">Ver todos</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4  colnal-slider-picture"><?php the_post_thumbnail('full'); ?></div>
                        <div class="col-lg-8 colnal-slider-contend-h">
                            <div>
                                <h1 class="colnal-name"><?php the_title(); ?></h1>
                                <h3 class="colnal-member-study-area"><?= isset($area_values[get_post_meta(get_the_ID(), 'colnal-member-study-area', true)]) ? $area_values[get_post_meta(get_the_ID(), 'colnal-member-study-area', true)] : "" ?></h3>
                                <h5 class="colnal-member-carrer"><?= get_post_meta(get_the_ID(), 'colnal-member-career', true); ?></h5>
                                <p class="colnal-slider-excerpt"><?= get_post_meta(get_the_ID(), 'colnal-member-entry-speech', true); ?></p>
                                <a href="<?php the_permalink(); ?>" class="colnal-btn-white">Ver Perfil</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                $item++;
                endwhile;
            endif;
            ?>
        </div>
    </div>
</div>
