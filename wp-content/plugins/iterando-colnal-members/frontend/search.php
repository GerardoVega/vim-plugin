<?php
/**
 * Created by PhpStorm.
 * User: rzerostern
 * Date: 7/23/19
 * Time: 5:27 PM
 */
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 colnal-sidebar">
            <form role="search" class="search search-form" action="<?php echo home_url( '/' ); ?>" method="GET" _lpchecked="1">
                <input type="hidden" name="post_type" value="vins">
                <div class="form-group col-lg-12 p-0">
                    <input type="text" class="search-field" name="s" value="">
                </div>
				<br>
                <button type="submit" class="search search-submit btn-lg btn-block">Buscar</button>
                <br>
            </form>
        </div>
        <div class="col-lg-9 colnal-main-container">
            <div class="row">
                <?php
                $area_values = array("1" => "Ciencias Biológicas y de la Salud", "2" => "Artes y Letras", "3" => "Ciencias Exactas", "4" => "Ciencias Sociales y Humanidades");
                $backgroundStyle = '';

                $loop = new WP_Query([
                    'post_type' => 'vins'
                ]);

                if($loop->have_posts()) :
                    while($loop->have_posts()): $loop->the_post();
                        ?>
                        <div class="col-lg-6 col-12 colnal-card">
                            <a class="colnal-member-permalink" href="<?php the_permalink(); ?>">
                                <div class="row">
                                    <?php
                                    $area = get_post_meta(get_the_ID(), 'colnal-member-study-area', true);
                                    switch($area) {
                                        case "1": $backgroundStyle = 'bio-tag-color'; break;
                                        case "2": $backgroundStyle = 'arts-tag-color'; break;
                                        case "3": $backgroundStyle = 'exact-tag-color'; break;
                                        case "4": $backgroundStyle = 'social-tag-color'; break;
                                    }
                                    ?>
                                    <div class="col-lg-6 colnal-card-info <?= $backgroundStyle; ?>">
                                        <h4><?php the_title(); ?></h4>
                                        <div class="colnal-bottom-description">
                                            <p class="study-area"><?= isset($area_values[get_post_meta(get_the_ID(), 'colnal-member-study-area', true)]) ? $area_values[get_post_meta(get_the_ID(), 'colnal-member-study-area', true)] : "" ?></p>
                                            <p class="study-area"><?= get_post_meta(get_the_ID(), 'colnal-member-career', true); ?></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 colnal-card-img">
                                        <?php the_post_thumbnail('full'); ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>
