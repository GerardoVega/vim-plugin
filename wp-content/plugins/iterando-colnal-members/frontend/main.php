<?php
/**
 * Created by PhpStorm.
 * User: rzerostern
 * Date: 7/23/19
 * Time: 5:27 PM
 */
?>
<div class="container-fluid content-principal-all-members">
    <div class="row">
        <div class="col-lg-12 colnal-sidebar"> 
            <form role="search" class="search search-form" action="/" method="POST" _lpchecked="1">
                <input type="hidden" name="post_type" value="vins">
                <div class="form-group row p-0">
                    <input type="text" class="search-field custom-int" name="s" value="" placeholder="Buscar integrantes">
                </div>
				<br>
				<div class="row">
					<button type="submit" id="hidden-submit" style="display: none">Hidden</button>
					<input type="hidden" name="page" id="nextPage" value="1">
					<button type="button" onclick="submitData()" class="search search-submit btn-lg btn-block" style="position:relative;">Buscar</button>
				</div>
                
            </form>
            <hr>      
        </div>
        <div class="col-lg-9 colnal-main-container">
            <br>
            <div class="row responsive-principal-colnal">
            <br>
            <?php
                $area_values = array("1" => "Ciencias Biológicas y de la Salud", "2" => "Artes y Letras", "3" => "Ciencias Exactas", "4" => "Ciencias Sociales y Humanidades");
                $backgroundStyle = '';

                $currentPage = get_query_var('paged');	

                $loop = new WP_Query([
                    'post_type' => 'integrantes',
                    'posts_per_page' => 10,
                    'meta_key' => 'colnal-member-first-last-name',
					'orderby' => 'meta_value',
				    'order'   => 'ASC',
                    'paged' => $currentPage
                ]);

                if($loop->have_posts()) :
            ?>
                    <div class="col-lg-12 int-numbers-ac">
                        <p style="margin-bottom:0px;"><?= $loop->found_posts; ?> Integrantes</p>
                    </div>
            <?php
                    while($loop->have_posts()): $loop->the_post();
            ?>
                    <div class="col-lg-6 col-12 colnal-card">
                        <a class="colnal-member-permalink" href="<?php the_permalink(); ?>">
                            <div class="row">
                                <?php
                                    $area = get_post_meta(get_the_ID(), 'colnal-member-study-area', true);
                                    switch($area) {
                                        case "1": $backgroundStyle = 'bio-tag-color'; break;
                                        case "2": $backgroundStyle = 'arts-tag-color'; break;
                                        case "3": $backgroundStyle = 'exact-tag-color'; break;
                                        case "4": $backgroundStyle = 'social-tag-color'; break;
                                    }
                                ?>
                                <div class="col-lg-6 colnal-card-info <?= $backgroundStyle; ?>">
                                    <?php if (get_post_meta(get_the_ID(), 'colnal-member-status', true) == "dead") { ?>
                                        <span class="colnal-member-dead"><img src="<?= plugins_url('/../assets/img/dead.png', __FILE__) ?>" alt="Fallecido"></span>
                                    <?php } ?>
                                    <h4 class="name-title"><?php the_title(); ?></h4>
                                    <div class="colnal-bottom-description">
                                        <p class="study-area"><?= isset($area_values[get_post_meta(get_the_ID(), 'colnal-member-study-area', true)]) ? $area_values[get_post_meta(get_the_ID(), 'colnal-member-study-area', true)] : "" ?></p>
                                        <p class="study-area career"><?= get_post_meta(get_the_ID(), 'colnal-member-career', true); ?></p>
                                    </div>
                                </div>
                                <div class="col-lg-6 order-first order-sm-last colnal-card-img">
                                    <?php the_post_thumbnail('full'); ?>
                                </div>
                            </div>
                        </a>
                    </div>
            <?php
                    endwhile;
            ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center pagination-members" style="padding: 10px;">
                            <?php
                                echo paginate_links([
                                    'total' => $loop->max_num_pages
                                ]);
                            ?>
                            </div>
                        </div>
                    </div>
            <?php
                endif; 
            ?>
            </div>
        </div>
    </div>
</div>
<script>
    function submitData()  { jQuery('#hidden-submit').click(); }

    jQuery(document).ready(function(e) {
        jQuery(".option").click(function(e){
            console.log(jQuery('input[name=status]:checked').val());
            jQuery("#statusField").val(jQuery('input[name=status]:checked').val());
            console.log(jQuery("#statusField").val());
            jQuery("#statusForm").submit();
        });
    })
</script>
